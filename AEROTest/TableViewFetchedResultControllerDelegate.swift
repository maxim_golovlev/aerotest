//
//  TableViewFetchedResultControllerDelegate.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class TableViewFetchedResultsControllerDelegate: NSObject, NSFetchedResultsControllerDelegate {
    
    private let tableView: UITableView
    private var blockOperations: [BlockOperation] = []
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    deinit {
        for operation in self.blockOperations {
            operation.start()
        }
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == .insert {
            blockOperations.append(BlockOperation.init(block: {
                self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
               // self.tableView.endUpdates()
            }))
            
        } else if type == .update {
            blockOperations.append(BlockOperation.init(block: {
                self.tableView.reloadRows(at: [indexPath!], with: .automatic)
               // self.tableView.endUpdates()
            }))
            
        } else if type == .move {
            blockOperations.append(BlockOperation.init(block: {
                self.tableView.deleteRows(at: [indexPath!], with: .automatic)
                self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
               // self.tableView.endUpdates()
            }))
        }
    }
    
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        for operation in self.blockOperations {
            operation.start()
        }
        
        self.tableView.endUpdates()
        self.blockOperations.removeAll(keepingCapacity: false)
    }

}
