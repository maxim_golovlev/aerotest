//
//  GalleryCell.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class GalleryCell: BaseTableCell {
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(GalleryImageCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        return cv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    override func setupViews() {
        
        backgroundColor = .white
        
        addSubview(titleLabel)
        addSubview(collectionView)
        addSubview(separator)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: titleLabel)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: collectionView)
        addConstraintsWithFormat("H:|-8-[v0]|", views: separator)
        addConstraintsWithFormat("V:|-8-[v0]->=8-[v1(150)]-10-[v2(1)]|", views: titleLabel, collectionView, separator)
    }
}

extension GalleryCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }
}

extension GalleryCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 150)
    }
}
