//
//  NewsItem+CoreDataClass.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class NewsItem: NSManagedObject {

    func initWithDict(dict: [String: AnyObject]) {
        
        self.imgUrl = dict["img"] as? String
        self.text = dict["description"] as? String
        
        if let id = dict["id"] as? String, let idString = Int32(id) {
            self.id = idString
        }
    }
}
