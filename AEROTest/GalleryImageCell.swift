//
//  GalleryImageCell.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class GalleryImageCell: BaseCollectionCell {

    let galleryImage: CustomImageView  = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        let shadowView = UIView()
        shadowView.backgroundColor = UIColor.init(white: 0.1, alpha: 0.5)
        iv.addSubview(shadowView)
        shadowView.fillSuperview()
        
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .white
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        galleryImage.image = nil
    }
    
    override func setupViews() {
        
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        addSubview(galleryImage)
        galleryImage.fillSuperview()
        
        addSubview(titleLabel)
        addConstraintsWithFormat("H:|-15-[v0]-15-|", views: titleLabel)
        addConstraintsWithFormat("V:|-15-[v0]", views: titleLabel)
    }
}
