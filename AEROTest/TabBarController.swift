//
//  ViewController.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    let catalogController: UINavigationController = {
        let vc = CatalogController()
        vc.tabBarItem.image = #imageLiteral(resourceName: "catalog")
        vc.title = "Каталог"
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }()
    
    let setsController: UINavigationController = {
        let vc = UIViewController()
        vc.tabBarItem.image = #imageLiteral(resourceName: "sets")
        vc.title = "Сеты"
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }()
    
    let actionsController: UINavigationController = {
        let vc = UIViewController()
        vc.tabBarItem.image = #imageLiteral(resourceName: "actions")
        vc.title = "Акции"
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }()
    
    let shopsController: UINavigationController = {
        let vc = UIViewController()
        vc.tabBarItem.image = #imageLiteral(resourceName: "shops")
        vc.title = "Магазины"
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }()
    
    let profileController: UINavigationController = {
        let vc = UIViewController()
        vc.tabBarItem.image = #imageLiteral(resourceName: "profile")
        vc.title = "Профиль"
        vc.view.backgroundColor = .white
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [catalogController, setsController, actionsController, shopsController, profileController]
    }
}

