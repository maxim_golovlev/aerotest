//
//  TematicCell.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class TematicCell: GalleryCell {
       
    var delegate:CollectionViewFetchedResultsControllerDelegate?
    
    lazy var fetchViewController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Tematic")
        fetchRequest.sortDescriptors = []
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: AppDelegate.currentContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchController
    }()
    
    override func setupViews() {
        super.setupViews()
               
        titleLabel.text = "Тематические подборки на любой случай"
      
        delegate = CollectionViewFetchedResultsControllerDelegate(collectionView: collectionView)
        fetchViewController.delegate = delegate
        
        do {
            try fetchViewController.performFetch()
            print(fetchViewController.sections![0].numberOfObjects)
        } catch let error {
            print(error)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchViewController.sections![0].objects?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! GalleryImageCell
        let tematic = fetchViewController.object(at: indexPath) as? Tematic
        cell.galleryImage.loadImageUsingUrlString(tematic?.imgUrl)
        cell.titleLabel.text = tematic?.title
        return cell
    }    
}

