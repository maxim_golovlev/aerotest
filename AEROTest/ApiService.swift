//
//  ApiService.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData

class ApiService {
    
    static let sharedService = ApiService()
    
    let urlString = "http://am.aeroidea.ru/api/catalog"
    
    func fetchCatalog(completion: @escaping ([Section], [Tematic], [NewsItem]) -> ()) {
        
        if let url = URL(string: urlString) {
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                self.createModels(withData: data, completion: { (sections, tematics, news) in
                    completion(sections, tematics, news)
                })
                
            }).resume()
        }
    }
    
    func createModels(withData data: Data?, completion: @escaping ([Section], [Tematic], [NewsItem]) -> ()) {
        
        guard let data = data else { return }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {                

                let persistentContainer = AppDelegate.appPersistentContainer
                
                var sections = [Section]()
                var tematics = [Tematic]()
                var news = [NewsItem]()
                
                persistentContainer.performBackgroundTask({ (context) in
                    
                    if let catalog = json["catalog"] as? [String: AnyObject] {
                        
                        if let sectionDicts = catalog["sections"] as? [[String: AnyObject]] {
                            for sectionDict in sectionDicts {
                                if let id = sectionDict["sectionID"] as? String {
                                    if let section: Section = Section.createOrReturn(withId: Int(id), context: context) {
                                        section.initWithDict(dict: sectionDict)
                                        sections.append(section)
                                    }
                                }
                            }
                        }
                        
                        if let tematicDicts = catalog["tematicSets"] as? [[String: AnyObject]] {
                            for tematicDict in tematicDicts {
                                if let id = tematicDict["id"] as? String {
                                    if let tematic: Tematic = Tematic.createOrReturn(withId: Int(id), context: context) {
                                        tematic.initWithDict(dict: tematicDict)
                                        tematics.append(tematic)
                                    }
                                }
                            }
                            
                        }
                        
                        if let newsItemDicts = catalog["news"] as? [[String: AnyObject]] {
                            for newsItemDict in newsItemDicts {
                                if let id = newsItemDict["id"] as? String {
                                    if let newsItem: NewsItem = NewsItem.createOrReturn(withId: Int(id), context: context) {
                                        newsItem.initWithDict(dict: newsItemDict)
                                        news.append(newsItem)
                                    }
                                }
                            }
                        }
                        
                        do {
                            try context.save()
                            completion(sections, tematics, news)
                        } catch {
                            fatalError("Failure to save context: \(error)")
                        }
                    }
                })
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
    }    
}
