//
//  Section+CoreDataClass.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Section: NSManagedObject {

    func initWithDict(dict: [String: AnyObject]) {
        
        self.title = dict["name"] as? String
        
        if let id = dict["sectionID"] as? String, let idString = Int32(id) {
            self.id = idString
        }
    }
    
}
