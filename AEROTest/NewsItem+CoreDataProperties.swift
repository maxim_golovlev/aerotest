//
//  NewsItem+CoreDataProperties.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension NewsItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsItem> {
        return NSFetchRequest<NewsItem>(entityName: "NewsItem");
    }

    @NSManaged public var id: Int32
    @NSManaged public var imgUrl: String?
    @NSManaged public var text: String?

}
