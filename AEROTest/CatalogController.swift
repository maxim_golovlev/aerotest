//
//  CatalogController.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class CatalogController: UIViewController {

    let sectionCellId = "sectionCellId"
    let tematicCellId = "tematicCellId"
    let newsCellId = "newsCellId"
    
    var sections: [Section]?
    
    lazy var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.register(SectionCell.self, forCellReuseIdentifier: self.sectionCellId)
        tableView.register(TematicCell.self, forCellReuseIdentifier: self.tematicCellId)
        tableView.register(NewsItemCell.self, forCellReuseIdentifier: self.newsCellId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = lightGray
        return tableView
    }()
    
    lazy var searchBar: UISearchBar = {
        let bar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 48))
        bar.delegate = self
        bar.placeholder = "Поиск по каталогу"
        return bar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "burger"), style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "trace"), style: .plain, target: self, action: #selector(handleBuy))
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "alco_logo"))
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleResign)))
        
        self.setupViews()
        
        ApiService.sharedService.fetchCatalog { (sections, tematics, news) in
            self.sections = sections
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func setupViews() {
        
        view.addSubview(tableView)
        tableView.fillSuperview()
        
        tableView.tableHeaderView = searchBar
        tableView.tableFooterView = UIView()
    }
    
    func handleResign() {
        searchBar.resignFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleMenu() {

    }
    
    func handleBuy() {
        
    }
}

extension CatalogController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tableCell: UITableViewCell
        
        switch indexPath.row {
        case 0:
            tableCell = tableView.dequeueReusableCell(withIdentifier: sectionCellId, for: indexPath)
        case 1:
            tableCell = tableView.dequeueReusableCell(withIdentifier: tematicCellId, for: indexPath) as! TematicCell
        case 2:
            tableCell = tableView.dequeueReusableCell(withIdentifier: newsCellId, for: indexPath) as! NewsItemCell
        default:
            tableCell = UITableViewCell.init(style: .default, reuseIdentifier: "cellId")
        }
        return tableCell
    }
}

extension CatalogController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return CGFloat(sections?.count ?? 0) * 44 + 8 + 8
        default:
            return 220
        }
    }
}

extension CatalogController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

    }
}

