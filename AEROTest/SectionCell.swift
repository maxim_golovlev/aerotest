//
//  SectionCell.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class SectionCell: BaseTableCell {
    
    let cellId = "cellId"
    var blockOperations = [BlockOperation]()
    var delegate:TableViewFetchedResultsControllerDelegate?

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: self.cellId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = lightGray
        tableView.isScrollEnabled = false
        tableView.layer.cornerRadius = 10
        tableView.layer.masksToBounds = true
        return tableView
    }()
    
    lazy var fetchViewController: NSFetchedResultsController<NSFetchRequestResult> = {
        let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Section")
        fetchRequest.sortDescriptors = []
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: currentContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchController
    }()
    
    override func setupViews() {
        
        backgroundColor = lightGray
        
        addSubview(tableView)
        tableView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        delegate = TableViewFetchedResultsControllerDelegate(tableView: tableView)
        fetchViewController.delegate = delegate
        
        do {
            try fetchViewController.performFetch()
            print(fetchViewController.sections![0].numberOfObjects)
        } catch let error {
            print(error)
        }
    }
}

extension SectionCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchViewController.sections![0].objects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "arrow"))
        let section = fetchViewController.object(at: indexPath) as? Section
        cell.textLabel?.text = section?.title
        
        return cell
    }
}

extension SectionCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
