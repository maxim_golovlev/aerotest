//
//  CollectionViewFetchedResultsControllerDelegate.swift
//  AEROTest
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class CollectionViewFetchedResultsControllerDelegate: NSObject, NSFetchedResultsControllerDelegate {
    
    private let collectionView: UICollectionView
    private var blockOperations: [BlockOperation] = []
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
    }

    deinit {
        blockOperations.forEach { $0.cancel() }
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            let op = BlockOperation { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            blockOperations.append(op)
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            let op = BlockOperation { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            blockOperations.append(op)
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            let op = BlockOperation { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            blockOperations.append(op)
            
        case .delete:
            guard let indexPath = indexPath else { return }
            let op = BlockOperation { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
            blockOperations.append(op)
            
        }
    }
    
    func controller(controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
            
        case .insert:
            let op = BlockOperation { [weak self] in self?.collectionView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet) }
            blockOperations.append(op)
            
        case .update:
            let op = BlockOperation { [weak self] in self?.collectionView.reloadSections(NSIndexSet(index: sectionIndex) as IndexSet) }
            blockOperations.append(op)
            
        case .delete:
            let op = BlockOperation { [weak self] in self?.collectionView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet) }
            blockOperations.append(op)
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
        }, completion: { finished in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
}
