//
//  ManagedObject+Ext.swift
//  AEROTest
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    class func createOrReturn<T: NSManagedObject>(withId id: Int?, context: NSManagedObjectContext) -> T? {
        
        var objectToReturn: T? = nil
        let entityName = String(describing: T.self)
        
        guard let id = id else { return objectToReturn }
        
        if let oldObject: T = attemptToFetch(withId: id, context: context) {
            objectToReturn = oldObject
            
        } else {
            let newObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: AppDelegate.currentContext) as! T
            objectToReturn = newObject
        }
        
        return objectToReturn
    }
    
    
    class func attemptToFetch<T: NSManagedObject>(withId id: Int, context: NSManagedObjectContext) -> T? {
        
        let object: T? = fetch(predicate: NSPredicate(format: "id = %d", id), context: context)?.first
        return object
    }
    
    class func fetch<T: NSManagedObject>(predicate: NSPredicate? = nil, context: NSManagedObjectContext) -> [T]? {
        
        var objects:[T]?
        
        do {
            let entityName = String(describing: T.self)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.predicate = predicate
            objects = try context.fetch(fetchRequest) as? [T]
            return objects
            
        } catch let error {
            print(error)
        }
        
        return nil
    }
}
